<?php
$users = array(
    "Max" => "11111111",
    "Michel B" => "14881488",
);

$response = [
    "error" =>''
];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = json_decode(file_get_contents('php://input'), true);
    $firstName = $data['first-name'];
    $password = $data['password'];
    if ($users[$firstName] != $password)
    {
        $response["error"] = "Incorrect username or password!";
        http_response_code(400);
    }
    
    echo json_encode($response);
}
?>